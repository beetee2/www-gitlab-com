---
layout: markdown_page
title: "Getting Help for GitLab"
---

* The [GitLab update page](https://about.gitlab.com/update/) helps you with updating your GitLab instance.
* [Maintenance policy](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/MAINTENANCE.md) specifies what versions are supported.
* [GitLab Community Forum](https://forum.gitlab.com/) is the place to share, ask and discuss everything related to GitLab.
* [Troubleshooting guide](https://github.com/gitlabhq/gitlab-public-wiki/wiki/Trouble-Shooting-Guide) contains solutions to common problems.
* [Mailing list](https://groups.google.com/forum/#!forum/gitlabhq) and [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab) are the best places to ask questions. Examples may be: permission denied errors, invisible repos, can't clone/pull/push or with web hooks that don't fire. Please search for similar issues before posting your own, there's a good chance somebody else had the same issue you have now and has resolved it. There are many helpful GitLab users there who may be able to help you quickly. If your particular issue turns out to be a bug, it will soon find its way toward a fix.
* [Feature request forum](http://feedback.gitlab.com) is the place to propose and discuss new features for GitLab.
* [Contributing guide](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md) describes how to submit merge requests and issues. Pull requests and issues not in line with the guidelines in this document will be closed.
* [A subscription](https://about.gitlab.com/subscription/) allows you to [contact](https://about.gitlab.com/contact/) our GitLab service engineers to solve your problem.
* [Consultancy](https://about.gitlab.com/consultancy/) from the GitLab experts for installations, upgrades and customizations.
* [#gitlab IRC channel](https://webchat.freenode.net/?channels=gitlab) on Freenode to get in touch with other GitLab users and get help. It is managed by James Newton (newton), Drew Blessing (dblessing), and Sam Gleske (sag47).
* [The GitLab Cookbook](https://www.packtpub.com/application-development/gitlab-cookbook), written by core team member Jeroen van Baarsen, is the most comprehensive book about GitLab.
    * Version two of the [Pro Git book](http://git-scm.com/book/en/v2) has a [section about GitLab.](http://git-scm.com/book/en/v2/Git-on-the-Server-GitLab)
    * O'Reilly Media is working on a [book](http://shop.oreilly.com/product/0636920034520.do), as well as [videos](http://shop.oreilly.com/product/0636920034872.do?code=WKGTVD) about git and GitLab. They also provide a [free video about creating a GitLab account](http://player.oreilly.com/videos/9781491912003?toc_id=194077).
* [Gitter chat room](https://gitter.im/gitlabhq/gitlabhq#) here you can ask questions when you need help.
* [GitLab Youtube Channel](https://www.youtube.com/channel/UCnMGQ8QHMAnVIsI3xJrihhg) where you can see video's of features and installation options.
* [GitLab documentation](http://doc.gitlab.com/ce/) has articles about the functionality in GitLab for users, administrators and developers.
* [The issue tracker](https://gitlab.com/gitlab-org/gitlab-ce/issues) lists errors and bugs people are experiencing. Please read the [contributing guidelines](https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md#issue-tracker) before posting on the issue tracker.
* [GitLab.com](https://about.gitlab.com/gitlab-com/) users can use the [GitLab.com Support Forum](https://gitlab.com/gitlab-com/support-forum/issues) if they do not have a support package.
